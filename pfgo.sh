#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

red(){
   echo -e "\033[31m\033[01m$1\033[0m"
}
green(){
   echo -e "\033[32m\033[01m$1\033[0m"
}
yellow(){
   echo -e "\033[33m\033[01m$1\033[0m"
}
blue(){
   echo -e "\033[36m\033[01m$1\033[0m"
}
white(){
   echo -e "\033[1;37m\033[01m$1\033[0m"
}
bblue(){
   echo -e "\033[1;34m\033[01m$1\033[0m"
}
rred(){
   echo -e "\033[1;35m\033[01m$1\033[0m"
}

if [[ $EUID -ne 0 ]]; then
   yellow " 请以root模式运行脚本。"
   exit 1
fi

if [[ -f /etc/redhat-release ]]; then
   release="Centos"
elif cat /etc/issue | grep -q -E -i "debian"; then
   release="Debian"
elif cat /etc/issue | grep -q -E -i "ubuntu"; then
   release="Ubuntu"
elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
   release="Centos"
elif cat /proc/version | grep -q -E -i "debian"; then
   release="Debian"
elif cat /proc/version | grep -q -E -i "ubuntu"; then
   release="Ubuntu"
elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
   release="Centos"
fi

if ! type curl >/dev/null 2>&1; then
   yellow "curl 未安装，安装中 "
   sudo apt update && apt install curl -y 
else
   green "curl 已安装，继续 "
fi

if ! type wget >/dev/null 2>&1; then
   yellow "wget 未安装 安装中 "
   sudo apt update && apt install wget -y 
else
   green "wget 已安装，继续 "
fi

if ! type lsb_release >/dev/null 2>&1; then
   yellow "lsb-release 未安装 安装中 "
   sudo apt update && apt install lsb-release -y 
else
   green "lsb-release 已安装，继续 "
fi

if ! type bc >/dev/null 2>&1; then
   yellow "bc 未安装，安装中 "
   sudo apt update && apt install bc -y 
else
   green "bc 已安装，继续 "
fi

reading(){ read -rp "$(green "$1")" "$2"; }
download_url="https://mirrors.pku.edu.cn/centos-stream/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-dvd1.iso"

bit=`uname -m`
version=`uname -r | awk -F "-" '{print $1}'`
main=`uname -r | awk -F . '{print $1 }'`
minor=`uname -r | awk -F . '{print $2}'`
rv4=`ip a | grep global | awk 'NR==1 {print $2}' | cut -d'/' -f1`
rv6=`ip a | grep inet6 | awk 'NR==2 {print $2}' | cut -d'/' -f1`
netname=`ip a | grep global | awk 'NR==1 {print $NF}' | cut -d'/' -f1`
op=`lsb_release -d | awk -F ':' '{print $2}'`
kernel=`lsb_release -sr | awk -F . '{print $1 }'`
vi=`systemd-detect-virt`
net_congestion_control=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
net_qdisc=`cat /proc/sys/net/core/default_qdisc | awk '{print $1}'`
Mem=`grep MemTotal /proc/meminfo | awk -F ':' '{print $2}' | awk '{print $1}'`
totalMem=`echo "scale=2; $Mem/1024/1024" | bc`

if [[ ${vi} == "kvm" ]]; then
   green " ---VPS扫描中---> "
elif [[ ${vi} == "xen" ]]; then
   green " ---VPS扫描中---> "
elif [[ ${vi} == "microsoft" ]]; then
   green " ---VPS扫描中---> "
elif [[ ${vi} == "vmware" ]]; then
   green " ---VPS扫描中---> "
elif [[ ${vi} == "none" || ${bit} == "aarch64" ]]; then
   green " ---VPS扫描中---> "
elif [[ ${vi} == "openvz" ]]; then
   green " ---VPS扫描中---> "
elif [[ ${vi} == "lxc" ]]; then
   green " ---VPS扫描中---> "
else
   yellow " 虚拟架构类型 - $vi "
   yellow " 对此vps架构不支持，脚本安装自动退出！"
   exit 1
fi

yellow " VPS相关信息如下："
   white " -----------------------------------------------" 
   blue " 操作系统名称 -$op "
   blue " 系统版本号   - $kernel "
   blue " 系统内核版本 - $version " 
   blue " CPU架构名称  - $bit "
   blue " 虚拟架构类型 - $vi "
   blue " 拥塞算法 - $net_congestion_control "
   blue " 队列算法 - $net_qdisc "
   blue " 系统内存 - $totalMem GB"
   white " -----------------------------------------------" 
sleep 1s

function bbr(){
   if [ ! -f "/etc/sysctl.conf" ]; then
      touch /etc/sysctl.conf
   fi
   if [ ! -f "/etc/sysctl.d/99-sysctl.conf" ]; then
      touch /etc/sysctl.d/99-sysctl.conf
   fi
   find  /etc/sysctl.d/ -type f -not \( -name '99-sysctl.conf' -or -name 'README.sysctl' \) -delete

   sed -i '/^#/!d' /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   echo "fs.file-max=1000000
fs.inotify.max_user_instances=8192
net.ipv4.tcp_max_syn_backlog=8192
net.ipv4.tcp_syncookies=1
net.ipv4.tcp_no_metrics_save=1
net.ipv4.tcp_ecn=0
net.ipv4.tcp_frto=0
net.ipv4.tcp_mtu_probing=0
net.ipv4.tcp_rfc1337=0
net.ipv4.tcp_sack=1
net.ipv4.tcp_fack=1
net.ipv4.tcp_window_scaling=1
net.ipv4.tcp_adv_win_scale=1
net.ipv4.tcp_moderate_rcvbuf=1
net.ipv4.ip_forward=1
net.core.somaxconn=3276800
net.core.optmem_max=81920
net.core.wmem_default=131072
net.core.rmem_default=131072
net.core.wmem_max=16777216
net.core.rmem_max=16777216
net.ipv4.tcp_mem=1572864 2097152 3145728
net.ipv4.tcp_wmem=4096 131072 16777216
net.ipv4.tcp_rmem=4096 131072 16777216
net.netfilter.nf_conntrack_tcp_timeout_time_wait=60
net.netfilter.nf_conntrack_tcp_timeout_established=86400
net.netfilter.nf_conntrack_max=1048576
net.ipv4.ip_local_port_range = 40000 65535
net.core.default_qdisc=fq
net.ipv4.tcp_congestion_control=bbr" | tee -a /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf

   if [[ ${totalMem//.*/} -lt 4 ]]; then    #<4GB 1G_3G_8G
      sed -i "s#.*net.ipv4.tcp_mem=.*#net.ipv4.tcp_mem=262144 786432 2097152#g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
      tcpmeminfo="应用4GB内存TCP方案"
   elif [[ ${totalMem//.*/} -ge 4 && ${totalMem//.*/} -lt 7 ]]; then    #6GB 3G_4G_8G
      sed -i "s#.*net.ipv4.tcp_mem=.*#net.ipv4.tcp_mem=786432 1048576 2097152#g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
      tcpmeminfo="应用6GB内存TCP方案"
   elif [[ ${totalMem//.*/} -ge 7 && ${totalMem//.*/} -lt 11 ]]; then    #8GB 3G_4G_8G
      sed -i "s#.*net.ipv4.tcp_mem=.*#net.ipv4.tcp_mem=786432 1048576 2097152#g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
      tcpmeminfo="应用8GB内存TCP方案"
   elif [[ ${totalMem//.*/} -ge 11 && ${totalMem//.*/} -lt 15 ]]; then    #12GB 4G_6G_12G
      sed -i "s#.*net.ipv4.tcp_mem=.*#net.ipv4.tcp_mem=1048576 1572864 3145728#g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
      tcpmeminfo="应用12GB内存TCP方案"
   elif [[ ${totalMem//.*/} -ge 15 && ${totalMem//.*/} -lt 22 ]]; then     #16GB 6G_8G_12G
      sed -i "s#.*net.ipv4.tcp_mem=.*#net.ipv4.tcp_mem=1572864 2097152 3145728#g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
      tcpmeminfo="应用16GB内存TCP方案"
   elif [[ ${totalMem//.*/} -ge 22 ]]; then     #>24GB 12G_16G_20G
      sed -i "s#.*net.ipv4.tcp_mem=.*#net.ipv4.tcp_mem=3145728 4194304 5242880#g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
      tcpmeminfo="应用超大内存TCP方案"
   fi

   if lsmod | grep -wq nf_conntrack; then
      nfconninfo="nf_conntrack_loaded"
   else
      nfconninfo="nf_conntrack_not_loaded"
      sed -i '/net.netfilter.nf_conntrack/d' /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   fi

   echo "1000000" > /proc/sys/fs/file-max
   sed -i '/required pam_limits.so/d' /etc/pam.d/common-session
   echo "session required pam_limits.so" >>/etc/pam.d/common-session
   sed -i '/ulimit -SHn/d' /etc/profile
   echo "ulimit -SHn 1000000" >>/etc/profile
   ulimit -SHn 1000000 && ulimit -c unlimited
   echo "*     soft   nofile    1048576
*     hard   nofile    1048576
*     soft   nproc     1048576
*     hard   nproc     1048576
*     soft   core      1048576
*     hard   core      1048576
*     hard   memlock   unlimited
*     soft   memlock   unlimited">/etc/security/limits.conf

   sed -i '/DefaultTimeoutStartSec/d' /etc/systemd/system.conf
   sed -i '/DefaultTimeoutStopSec/d' /etc/systemd/system.conf
   sed -i '/DefaultRestartSec/d' /etc/systemd/system.conf
   sed -i '/DefaultLimitCORE/d' /etc/systemd/system.conf
   sed -i '/DefaultLimitNOFILE/d' /etc/systemd/system.conf
   sed -i '/DefaultLimitNPROC/d' /etc/systemd/system.conf
   echo "#DefaultTimeoutStartSec=90s
DefaultTimeoutStopSec=30s
#DefaultRestartSec=100ms
DefaultLimitCORE=infinity
DefaultLimitNOFILE=20480000
DefaultLimitNPROC=20480000">>/etc/systemd/system.conf
   systemctl daemon-reload
   systemctl daemon-reexec
   sysctl -p

   green $nfconninfo
   green $tcpmeminfo
   green "Socket端口范围设置为40000-65535"
   green "如添加过其他BBR请重启服务器生效"
   before_show_menu
}

function task(){
   green "设置时区到GTM+800"
   timedatectl set-timezone Asia/Shanghai
   timedatectl
   green "设置每隔5天的4:35重启pfgo"
   sed -i '/PortForwardGo/d' /var/spool/cron/crontabs/root
   crontab -l | { cat; echo "35 4 */5 * * echo > /opt/PortForwardGo/run.log;echo > /opt/PortForwardGo/firewall.log;systemctl restart PortForwardGo"; } | crontab -
   green "当前任务计划:"
   crontab -l
   before_show_menu
}

function closetask(){
   sed -i '/PortForwardGo/d' /var/spool/cron/crontabs/root
   green "当前任务计划:"
   crontab -l
   before_show_menu
}

function bandwidth(){
cat > /etc/systemd/system/bandwidth.timer << EOF
[Unit]
Description=Run the Bandwidth every 60 minutes

[Timer]
OnBootSec=15min
OnUnitActiveSec=60min
Unit=bandwidth.service
[Install]
WantedBy=timers.target
EOF

# 配置Speedtest
cat > /etc/systemd/system/bandwidth.service <<EOF
[Unit]
Description=Bandwidth

[Service]
ExecStart=speedtest
EOF

reading "需要自定义带宽占用的设置吗? 默认每隔60分钟最大200mbps下载10分钟 (y/[n]) " answer
if [ "$answer" == "y" ]; then
	reading "输入你需要间隔的时长(以分钟为单位，例如60分钟输入60): " interval
	sed -i "s/^OnUnitActiveSec.*/OnUnitActiveSec=${interval}min/" /etc/systemd/system/bandwidth.timer
   sed -i "s/^Description.*/Description=Run the Bandwidth every ${interval}min/" /etc/systemd/system/bandwidth.timer
   reading "输入你需要的带宽大小(以mbps为单位，例如200mbps输入200): " rate_mbps
	rate=$(( rate_mbps * 100000 ))
   reading "输入你需要请求的时长(以分钟为单位，例如10分钟输入10): " timeout
   sed -i "s#.*ExecStart=.*#ExecStart=timeout ${timeout}m wget '$download_url' --limit-rate=$rate -O /dev/null#g" /etc/systemd/system/bandwidth.service
else
   echo -e "\n使用默认配置，每隔60分钟最大200mbps下载10分钟" 
   sed -i "s#.*ExecStart=.*#ExecStart=timeout 10m wget '$download_url' --limit-rate=20000000 -O /dev/null#g" /etc/systemd/system/bandwidth.service
fi

systemctl daemon-reload
systemctl enable bandwidth.timer
if systemctl restart bandwidth.timer ; then
   echo -e "\n定时下载脚本安装成功"
   echo -e "\n输入 systemctl status bandwidth.timer 查看任务"
else
   restorecon /etc/systemd/system/bandwidth.timer
   restorecon /etc/systemd/system/bandwidth.service
   systemctl enable bandwidth.timer
   systemctl restart bandwidth.timer
   echo -e "\n定时下载脚本安装成功"
   echo -e "\n/etc/systemd/system/bandwidth.service"
   
   echo -e "\n输入 systemctl status bandwidth.timer 查看任务"
fi
before_show_menu
}


function closebandwidth(){
   systemctl stop bandwidth
   systemctl stop bandwidth.timer
   systemctl disable bandwidth.timer
   before_show_menu
}

function banping(){
   sed -i '/net.ipv4.icmp_echo_ignore_all/d' /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   sed -i '/net.ipv4.icmp_echo_ignore_broadcasts/d' /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   echo "net.ipv4.icmp_echo_ignore_all=1" | tee -a /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   echo "net.ipv4.icmp_echo_ignore_broadcasts=1" | tee -a /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   sysctl -p
   before_show_menu
}

function openping(){
   sed -i "s/net.ipv4.icmp_echo_ignore_all=1/net.ipv4.icmp_echo_ignore_all=0/g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   sed -i "s/net.ipv4.icmp_echo_ignore_broadcasts=1/net.ipv4.icmp_echo_ignore_broadcasts=0/g" /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf
   sysctl -p
   before_show_menu
}


function before_show_menu() {
   echo && echo -n -e "${yellow}* 按回车返回主菜单 *${plain}" && read temp
   start_menu
}

#主菜单
function start_menu(){
    white " ===============================================================================================" 

    green " 1. 开启bbr+fq加速 "

    green " 2. 添加重启PFGO任务计划 "

    green " 3. 删除重启PFGO任务计划 "

    green " 4. 开启自动下载使上下不对等 "

    green " 5. 关闭自动下载使上下对等 "

    green " 6. 屏蔽ICMP "

    green " 7. 开放ICMP "

    green " 0. 退出脚本 "

    white " ===============================================================================================" 

    echo
    read -p "请输入数字:" menuNumberInput
    case "$menuNumberInput" in
   1 )
      bbr
	;;
	2 )
      task
	;;
	3 )
      closetask
	;;
	4 )
      bandwidth
	;;
	5 )
      closebandwidth
	;;
	6 )
      banping
	;;
	7 )
      openping
	;;
	0 )
      exit 1
	;;
      esac
}


if [ "$1" = "1" ];then
   bbr
elif [ "$1" = "2" ];then
   task
else
   start_menu "first"
fi
